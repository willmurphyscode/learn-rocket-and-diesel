-- Your SQL goes here
CREATE TABLE switches (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  powered_on BOOLEAN NOT NULL DEFAULT 'f'
)