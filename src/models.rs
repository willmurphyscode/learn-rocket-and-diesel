use diesel;
use self::diesel::prelude::*;
use diesel::pg::PgConnection; 
use db; 


#[derive(Queryable)]
#[derive(Serialize, Deserialize)]
pub struct Switch {
    pub id: i32,
    pub name: String,
    pub powered_on: bool,
}

use super::schema::switches;

#[derive(Insertable)]
#[table_name="switches"]
pub struct NewSwitch {
    pub name: String,
    pub powered_on: bool,
}

pub fn switch_the_switch(switch_id: i32, turned_on: bool) -> Result<Switch, diesel::result::Error> {
    use schema::switches::dsl::*;
    use schema::switches;

    let connection = db::establish_connection();
    let switch = diesel::update(switches.find(switch_id))
        .set(powered_on.eq(turned_on))
        .get_result::<Switch>(&connection);
    switch
}

pub fn create_switch(switch_name: String, powered_on: bool) -> Switch {
    use schema::switches;
    let connection = db::establish_connection();
    let new_switch = NewSwitch { name: switch_name, powered_on: powered_on };
    diesel::insert(&new_switch)
        .into(switches::table)
        .get_result(&connection)
        .expect("Error saving new switch")
}

pub fn all_switches() -> Vec<Switch> {
    use schema::switches::dsl::*;

    let connection = db::establish_connection();
    switches
        .limit(500)
        .load::<Switch>(&connection)
        .expect("Error loading switches")

}

pub fn all_by_powered_state(powered_on: bool) -> Vec<Switch> {
    use schema::switches::dsl::*;

    let connection = db::establish_connection();

    switches.filter(powered_on.eq(powered_on))
        .limit(500)
        .load::<Switch>(&connection)
        .expect("Error loading switches")
}