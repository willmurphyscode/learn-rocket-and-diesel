#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_codegen;
extern crate dotenv;

extern crate rocket;

extern crate serde;
#[macro_use]
extern crate serde_json;

#[macro_use] 
extern crate serde_derive;
extern crate rocket_contrib;


use diesel::pg::PgConnection; 
use rocket::http::RawStr;
mod db;

use self::diesel::prelude::*;

pub mod schema;
pub mod models;

use models::Switch;
use rocket_contrib::Json;

#[get("/switches/all")]
fn switches_all() -> Json<Vec<Switch>> {
    let results = models::all_switches(); 
    Json(results)
}

#[get("/switches/all/on")]
fn switches_all_on() -> Json<Vec<Switch>> {
    let results = models::all_by_powered_state(true);
    Json(results)
}

#[get("/switches/all/off")]
fn switches_all_off() -> Json<Vec<Switch>> {
    let results = models::all_by_powered_state(false);
    Json(results)
}

#[post("/switches/new/<n>")]
fn switches_new(n: &RawStr) -> Json<Switch> {
    let switch_name = n.as_str().to_string();
    let created_switch = models::create_switch(switch_name, false);
    Json(created_switch)
}

#[post("/switches/turn_on/<switch_id>")]
fn switches_turn_on(switch_id: i32) -> Result<Json<Switch>, String> {
    if let Ok(switch) = models::switch_the_switch(switch_id, true) {
        return Ok(Json(switch))
    };
    Err(json!({ "error" : "nothing found for that id"}).to_string())
}

#[post("/switches/turn_off/<switch_id>")]
fn switches_turn_off(switch_id: i32) -> Result<Json<Switch>, String> {
    if let Ok(switch) = models::switch_the_switch(switch_id, false) {
        return Ok(Json(switch))
    };
    Err(json!({ "error" : "nothing found for that id"}).to_string())
}


fn main() {
    let connection = db::establish_connection();
    rocket::ignite().mount("/", routes![
            switches_all,
            switches_all_on,
            switches_all_off,
            switches_new,
            switches_turn_on,
            switches_turn_off,
            ])
        .launch();
}
